//internal includes
#include "common.h"
#include "ShaderProgram.h"
#include "LiteMath.h"

//External dependencies
#define GLFW_DLL
#include <GLFW/glfw3.h>
#include <random>

static GLsizei WIDTH = 480, HEIGHT = 480;
using namespace LiteMath;


const int KEY_COUNT = 12;
const float EPS_DIV = 1e-5;

float3 default_pos(0, 0, 0);
float4 default_z(0, 1, 0, 0);
float4 default_y(0, 0, 1, 0);
float4 default_x(0, 0, 0, 1);
float4 default_euc(1, 0, 0, 0);


float3 g_camPos(0, 0, 0);
float4 g_locX = default_x;
float4 g_locY = default_y;
float4 g_locZ = default_z;
float4 g_euc_pos(1, 0, 0, 0);

float  cam_rot[3] = {0, 0, 0};
float default_rot[2] = {0, 0};
float main_scale = 3.0f;
bool camera_lock = true;
int    mx = 0, my = 0;
// W A S D R F 0 L U I Q E
char wasd[KEY_COUNT] = {};


void windowResize(GLFWwindow* window, int width, int height){
  WIDTH  = width;
  HEIGHT = height;
}

void centerCursor(GLFWwindow *window){
  if (camera_lock){
    mx = WIDTH / 2; my = HEIGHT / 2;
    glfwSetCursorPos(window, mx, my);
  }
}

float4 reflect4(float4 v, float4 a){
  float exy = a.x * v.y - a.y * v.x;
  float exz = a.x * v.z - a.z * v.x;
  float exw = a.x * v.w - a.w * v.x;
  float eyz = a.y * v.z - a.z * v.y;
  float eyw = a.y * v.w - a.w * v.y;
  float ezw = a.z * v.w - a.w * v.z;
  float dva = dot(v, a);
  float4 res = dva * a;
  res.x +=  exy * a.y + exz * a.z + exw * a.w;
  res.y += -exy * a.x + eyz * a.z + eyw * a.w;
  res.z += -exz * a.x - eyz * a.y + ezw * a.w;
  res.w += -exw * a.x - eyw * a.y - ezw * a.z;
  return -res;
}

float4 rotate4(float4 v, float4 a, float4 b){
  return reflect4(reflect4(v, a), b);
}

float3 euc2sphere(const float4 &p){
  float a1, a2, a3, sin1;
  a1 = acos(p.x);
  sin1 = sin(a1);
  a2 = acos(p.y / (sin1 + EPS_DIV));
  a3 = acos(p.z / (sin1 * sin(a2) + EPS_DIV));
  return float3(a1, a2, a3);
}

static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos){
  mx = int(xpos);
  my = int(ypos);
  // camera_active = true;
}

static void mouseMove(GLFWwindow* window, double xpos, double ypos){

  // xpos *= 0.005f;
  // ypos *= 0.005f;
  float a = 0.005;

  int x1 = int(xpos);
  int y1 = int(ypos);

  cam_rot[0] -= a*(y1 - my);	
  cam_rot[1] += a*(x1 - mx);

  mx = int(xpos);
  my = int(ypos);
}

void keyPress(GLFWwindow *window, int key, int scancode, int action, int mods){
  int keys[KEY_COUNT] = 
                      {GLFW_KEY_W, GLFW_KEY_A, GLFW_KEY_S, GLFW_KEY_D, 
                       GLFW_KEY_R, GLFW_KEY_F, GLFW_KEY_0, GLFW_KEY_L,
                       GLFW_KEY_U, GLFW_KEY_I, GLFW_KEY_Q, GLFW_KEY_E
                      };
  for (int i = 0; i < KEY_COUNT; i++){
    if (key == keys[i]){
      if (action == GLFW_PRESS)
        wasd[i] = 1;
      else if (action == GLFW_RELEASE)
        wasd[i] = 0;
      break;
    }
  }
  if (action == GLFW_RELEASE)
    if (key == GLFW_KEY_ESCAPE)
      glfwSetWindowShouldClose(window, 1);
    else if (key == GLFW_KEY_L)
      camera_lock = !camera_lock;

}

void updatePos1(){
  float speed = 0.005;
  float3 dir;
  float dx, dy, dz;
  dz = (wasd[0] - wasd[2])*speed;
  dx = (wasd[1] - wasd[3])*speed;
  dy = (wasd[5] - wasd[4])*speed;
  dir = float3(dx, dy, dz);
  main_scale += (wasd[9] - wasd[8])*speed;

  float4x4 camRotMatrix   = mul(rotate_Y_4x4(-cam_rot[1]), rotate_X_4x4(+cam_rot[0]));
  g_camPos = g_camPos - mul4x3(camRotMatrix, dir);
  // g_camPos = g_camPos - float3(0, dy, 0);

  // 0 IS PRESSED:
  if (wasd[6]){
    g_camPos = default_pos;
    cam_rot[0] = default_rot[0];
    cam_rot[1] = default_rot[1];
  }
}

void updatePos(){
  // TODO:
  float rad_3s = 3;
  //

  float speed = 0.05;
  float cam_tilt_speed = 0.01;
  float dx, dy, dz;
  dz = (wasd[0] - wasd[2])*speed;
  dx = (wasd[1] - wasd[3])*speed;
  dy = (wasd[5] - wasd[4])*speed;
  cam_rot[2] = (wasd[11] - wasd[10]) * cam_tilt_speed;
  main_scale += (wasd[9] - wasd[8])*speed;
  float4 dir = dx * g_locX + dy * g_locY + dz * g_locZ;
  float angle = length(dir) / rad_3s;
  float sa, ca;

  if (angle > 0){

    float4 dirn = normalize(dir);
    sa = sin(angle * 0.5);
    ca = cos(angle * 0.5);

    float4 new_euc = normalize(ca * g_euc_pos + sa * dirn);

    g_locX = normalize(rotate4(g_locX, g_euc_pos, new_euc));
    g_locY = normalize(rotate4(g_locY, g_euc_pos, new_euc));
    g_locZ = normalize(rotate4(g_locZ, g_euc_pos, new_euc));
    new_euc = normalize(rotate4(g_euc_pos, g_euc_pos, new_euc));

    // printf("(%f, %f, %f, %f); %f\n", g_locZ.x, g_locZ.y, g_locZ.z, g_locZ.w, length(g_locZ));
    // printf("(%f, %f, %f, %f); %f\n", g_locY.x, g_locY.y, g_locY.z, g_locY.w, length(g_locY));
    // printf("(%f, %f, %f, %f); %f\n", g_locX.x, g_locX.y, g_locX.z, g_locX.w, length(g_locX));
    // printf("(%f, %f, %f, %f);   \n", g_euc_pos.x, g_euc_pos.y, g_euc_pos.z, g_euc_pos.w);
    // printf("(%f, %f, %f, %f);   \n", new_euc.x, new_euc.y, new_euc.z, new_euc.w);


    g_euc_pos = new_euc;
    g_camPos = euc2sphere(g_euc_pos);
  }


  sa = sin(cam_rot[1]);
  ca = cos(cam_rot[1]);
  float4 newZ = ca * g_locZ + sa * g_locX;
  float4 newX = ca * g_locX - sa * g_locZ;

  sa = sin(cam_rot[0]);
  ca = cos(cam_rot[0]);

  float4 newY = ca * g_locY - sa * newZ;
  newZ = ca * newZ + sa * g_locY;

  sa = sin(cam_rot[2]);
  ca = cos(cam_rot[2]);
  
  g_locY = newY;

  newY = ca * g_locY + sa * newX;
  newX = ca * newX - sa * g_locY;

  g_locX = newX;
  g_locY = newY;
  g_locZ = newZ;


  // float4x4 camRotMatrix   = mul(rotate_Y_4x4(-cam_rot[1]), rotate_X_4x4(+cam_rot[0]));
  // g_camPos = g_camPos - mul4x3(camRotMatrix, dir);
  // g_camPos = g_camPos - float3(0, dy, 0);

  // 0 IS PRESSED:

  cam_rot[0] = 0;
  cam_rot[1] = 0;
  cam_rot[2] = 0;

  if (wasd[6]){
    g_camPos = default_pos;
    g_euc_pos = default_euc;
    g_locZ = default_z;
    g_locY = default_y;
    g_locX = default_x;
  }
}

int initGL()
{
	int res = 0;
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		return -1;
	}

	std::cout << "Vendor: "   << glGetString(GL_VENDOR) << std::endl;
	std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "Version: "  << glGetString(GL_VERSION) << std::endl;
	std::cout << "GLSL: "     << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	return 0;
}

int main(int argc, char** argv)
{
	if(!glfwInit())
    return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); 
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); 
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); 
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); 

  GLFWwindow*  window = glfwCreateWindow(WIDTH, HEIGHT, "xd",  nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	

  centerCursor(window);
  glfwSetCursorPosCallback (window, mouseMove);
  glfwSetWindowSizeCallback(window, windowResize);
  glfwSetKeyCallback(window, keyPress);

	glfwMakeContextCurrent(window); 
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

	if(initGL() != 0) 
		return -1;
	
  //Reset any OpenGL errors which could be present for some reason
	GLenum gl_error = glGetError();
	while (gl_error != GL_NO_ERROR)
		gl_error = glGetError();

	std::unordered_map<GLenum, std::string> shaders;
	shaders[GL_VERTEX_SHADER]   = "vertex.glsl";
	shaders[GL_FRAGMENT_SHADER] = "fragment.glsl";
	ShaderProgram program(shaders); GL_CHECK_ERRORS;

  glfwSwapInterval(1); // force 60 frames per second
  
  GLuint g_vertexBufferObject;
  GLuint g_vertexArrayObject;
  {
 
    float quadPos[] =
    {
      -1.0f,  1.0f,	// v0 - top left corner
      -1.0f, -1.0f,	// v1 - bottom left corner
      1.0f,  1.0f,	// v2 - top right corner
      1.0f, -1.0f	  // v3 - bottom right corner
    };

    g_vertexBufferObject = 0;
    GLuint vertexLocation = 0; // simple layout, assume have only positions at location = 0

    glGenBuffers(1, &g_vertexBufferObject);                                                        GL_CHECK_ERRORS;
    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBufferObject);                                           GL_CHECK_ERRORS;
    glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), (GLfloat*)quadPos, GL_STATIC_DRAW);     GL_CHECK_ERRORS;

    glGenVertexArrays(1, &g_vertexArrayObject);                                                    GL_CHECK_ERRORS;
    glBindVertexArray(g_vertexArrayObject);                                                        GL_CHECK_ERRORS;

    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBufferObject);                                           GL_CHECK_ERRORS;
    glEnableVertexAttribArray(vertexLocation);                                                     GL_CHECK_ERRORS;
    glVertexAttribPointer(vertexLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);                            GL_CHECK_ERRORS;

    glBindVertexArray(0);
  }

  program.StartUseShader();
  GLuint g_CubeMap = program.LoadCubeMapBMP(); GL_CHECK_ERRORS;
  program.StopUseShader();

  float time;

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
    updatePos();
    centerCursor(window);
		
    time = glfwGetTime();
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);               GL_CHECK_ERRORS;
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); GL_CHECK_ERRORS;

    program.StartUseShader();                           GL_CHECK_ERRORS;

    // float4x4 camRotMatrix   = mul(rotate_Y_4x4(-cam_rot[1]), rotate_X_4x4(+cam_rot[0]));
    // float4x4 camTransMatrix = translate4x4(g_camPos);
    // float4x4 rayMatrix      = mul(camTransMatrix, camRotMatrix);
    // program.SetUniform("g_rayMatrix", rayMatrix);

    // program.SetUniform("g_camPos", g_camPos);
    program.SetUniform("g_locX", g_locX);
    program.SetUniform("g_locY", g_locY);
    program.SetUniform("g_locZ", g_locZ);
    program.SetUniform("g_eucPos", g_euc_pos);

    program.SetUniform("g_time", time);
    program.SetUniform("g_screenWidth" , WIDTH);
    program.SetUniform("g_screenHeight", HEIGHT);
    program.SetUniform("main_scale", main_scale);
    program.SetUniformTexture("g_CubeMap", g_CubeMap);


    glViewport  (0, 0, WIDTH, HEIGHT);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear     (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // draw call
    //
    glBindVertexArray(g_vertexArrayObject); GL_CHECK_ERRORS;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);  GL_CHECK_ERRORS;  // The last parameter of glDrawArrays is equal to VS invocations
    

    // glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    program.StopUseShader();

		glfwSwapBuffers(window); 
	}

	glDeleteVertexArrays(1, &g_vertexArrayObject);
  glDeleteBuffers(1,      &g_vertexBufferObject);
  glDeleteTextures(1,     &g_CubeMap);

	glfwTerminate();

	return 0;
}
