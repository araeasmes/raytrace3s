#version 330

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define float4x4 mat4
#define float3x3 mat3

#define NOHIT 100
#define SPHERE 101
#define BOX   103
#define PLANE 104
#define CSG1 105
#define MANDELBULB 107
#define FRACT2 109
#define SMTHEL 113

const float PI        = 3.141592654f;
const float INV_PI    = 1.0f / PI;
const int sphere_cnt  = 2;
const int box_cnt     = 2;
const int mb_cnt      = 1;
const int csg1_cnt    = 1;
const int plane_cnt   = 1;
const int LIGHTS_CNT  = 2;
const float EPS_STOP  = 1e-3f;
const float EPS_DERIV = 1e-4f;
const float EPS_CURV  = 1e-2f;
const float EPS_DIV   = 1e-6f;
const int MAX_BOUNCES = 2;
const float MAX_ITER  = 500;
const float MAX_T     = 1e3f;
const float shadow_k  = 10 / EPS_STOP;
const int ALIASING    = 1;
float RADIUS_3S = 3;
float RADIUS_3S_INV = 1 / RADIUS_3S;


in float2 fragmentTexCoord;

struct Material{
  float3 ambient;
  float3 diffuse;
  float3 specular;
  float shiny;
  float reflection;
  float refraction;
};

struct LightSource{
  float3 pos;
  float3 color;
};

struct Sphere{
  Material mat;
  float4 center;
  float r;
};

struct Box{
  Material mat;
  float3 p;
  float3 b;
};

struct Plane{
  Material mat;
  float shift;
  float4 n;
};

struct Mandelbulb{
  float3 pos;
};

struct Csg{
  Material mat;
  float3 p;
  float3 b;
  float k;
};

struct RayHit{
  int obj_type;
  int obj_ind;
  float dist;
  float4 pos;
  float iter_cnt;
};

const Material BRASS = Material(
  float3(0.329412, 0.223529, 0.027451), 
  float3(0.780392, 0.568627, 0.113725), 
  float3(0.992157, 0.941176, 0.807843), 
  27.8974, 0.0, 0);

const Material CHROME = Material(
  float3(0.25, 0.25, 0.25), 
  float3(0.4, 0.4, 0.4), 
  float3(0.774597, 0.774597, 0.774597), 
  76.8, 0.9, 0);

const Material GOLD = Material(
  float3(0.24725, 0.2245, 0.0645), 
  float3(0.34615, 0.3143, 0.0903),
  float3(0.797357, 0.723991, 0.208006), 
  83.2, 0.0, 0);

const Material SILVER = Material(
  float3(0.24725, 0.2245, 0.2645), 
  float3(0.34615, 0.3143, 0.0903),
  float3(0.797357, 0.723991, 0.208006), 
  83.2, 0.3, 0);

const Material EMERALD = Material(
  float3(0.0215, 0.1745, 0.0215), 
  float3(0.07568, 0.61424, 0.07568),
  float3(0.633, 0.727811, 0.633), 
  76.8, 0.45, 0);

const Material MAT1 = Material(
  float3(0.3, 0.1, 0.3),
  float3(0.2, 0.1, 0.2),
  float3(0.4, 0.1, 0.3),
  20, 0, 0
  );

layout(location = 0) out vec4 fragColor;

uniform int g_screenWidth;
uniform int g_screenHeight;
uniform samplerCube g_CubeMap;

uniform float g_time;
uniform float main_scale;

uniform float3 g_LightSources[LIGHTS_CNT] = float3[](float3(PI*0.3, PI*0.3, PI), float3(-PI*0.3, PI*0.5, PI));
uniform float3 g_LightColors[LIGHTS_CNT]  = float3[](float3(1.0, 0.8, 1.0), float3(1.0, 0.0, 1.0)); 



Sphere spheres[sphere_cnt] = Sphere[](
  Sphere(SILVER, float4(0, 1, 0, 0), 2.5*INV_PI*0.5), 
  Sphere(CHROME, float4(0, 0, 1, 0), 3.5*INV_PI*0.5)
  );
Box boxes[box_cnt] = Box[](
  Box(EMERALD, float3(1, -1, -0.5), float3(0.5, 0.5, 0.5)), 
  Box(GOLD, float3(-0.5, 0.5, 0.5), float3(0.5, 0.5, 0.5))
  );

Plane planes[plane_cnt] = Plane[](
  Plane(CHROME, 0.3, normalize(float4(0, 1, 0, 1)))
  );

Csg csg1[csg1_cnt] = Csg[](
  Csg(CHROME, float3(1.75, 1.75, 1.75), float3(0.6, 0.6, 0.6), 1.45)
  );

// uniform float4x4 g_rayMatrix;
uniform float4   g_locX;
uniform float4   g_locY;
uniform float4   g_locZ;
uniform float4   g_eucPos;
uniform float4   g_bgColor = float4(0,0,1,1);


float4 EyeRayDir(float x, float y, float w, float h, float3 offset){
  float fov = PI / (2.0f); 
  float4 ray_dir;
  
  ray_dir = (x - w / 2.0f + offset.x) * g_locX + (y - h / 2.0f + offset.y) * g_locY + (w / tan(fov / 2.0f)) * g_locZ;
  
  return normalize(ray_dir);
}

float3 skyBox(float3 ray_dir){
  float3 v = ray_dir / max(abs(ray_dir.x), max(abs(ray_dir.y), abs(ray_dir.z)));
  return texture(g_CubeMap, ray_dir).xyz * (1.00);
  // return abs(normalize(ray_dir));
}

vec4 sphere2euc(float3 p){
  float x0, x1, x2, x3;
  x0 = cos(p.x);
  x1 = sin(p.x) * cos(p.y);
  x2 = sin(p.x) * sin(p.y) * cos(p.z);
  x3 = sin(p.x) * sin(p.y) * sin(p.z);
  return vec4(x0, x1, x2, x3);
}

vec3 euc2sphere(float4 p){
  float a1, a2, a3, sin1;
  a1 = acos(p.x);
  sin1 = sin(a1);
  a2 = acos(p.y / (sin1 + EPS_DIV));
  a3 = acos(p.z / (sin1 * sin(a2) + EPS_DIV));
  return vec3(a1, a2, a3);
}

// UNUSED (AND WRONG)
vec4 getRotVector(vec4 va, vec4 vb, vec4 v){
  float c12 = va.x * vb.y - va.y * vb.x;
  float c13 = va.x * vb.z - va.z * vb.x;
  float c14 = va.x * vb.w - va.w * vb.x;
  float c23 = va.y * vb.z - va.z * vb.y;
  float c24 = va.y * vb.w - va.w * vb.y;
  float c34 = va.z * vb.w - va.w * vb.z;
  float a1 = v.x;
  float a2 = v.y;
  float a3 = v.z;
  float a4 = v.w;

  vec4 res;
  res.x = a1*(c12*c12 + c13*c13 + c14*c14 - c23*c23 - c24*c24 - c34*c34)
        + a3*(-2*c12*c23 + 2*c14*c34)
        + a4*(-2*c12*c24 - 2*c13*c34);
  res.y = a1*(2*c13*c23 + 2*c14*c24)
        + a2*(c12*c12 + c23*c23 - c24*c24 - c13*c13 - c14*c14 - c34*c34)
        + a3*(2*c12*c13 + 2*c24*c34)
        + a4*(2*c12*c14 - 2*c23*c34);
  res.z = a2*(2*c12*c13)
        + a3*(c13*c13 + c23*c23 - c12*c12 - c14*c14 + c34*c34 - c24*c24)
        + a4*(2*c13*c14 + 2*c23*c24);
  res.w = a1*(-2*c12*c24)
        + a2*(2*c12*c14 - 2*c23*c24)
        + a3*(2*c13*c14 + 2*c23*c34)
        + a4*(-c12*c12 - c13*c13 - c23*c23 + c14*c14 + c24*c24 + c34*c34);
  return res;
}


// UNUSED (AND WRONG)
vec4 rotByVector(vec4 v, vec4 rv, float abdot, float angle){
  float theta = angle * 0.5;
  float sin2_theta = sin(theta);
  sin2_theta = sin2_theta * sin2_theta;
  float cos2_theta = 1 - sin2_theta;
  return -rv * sin2_theta + v * (cos2_theta -sin2_theta * abdot * abdot);
}

// UNUSED (AND WRONG)
vec4 rotInPlane(vec4 va, vec4 vb, float angle, vec4 v){
  float theta = angle * 0.5;
  float sin2_theta = sin(theta);
  sin2_theta = sin2_theta * sin2_theta;
  float cos2_theta = 1 - sin2_theta;

  float c12 = va.x * vb.y - va.y * vb.x;
  float c13 = va.x * vb.z - va.z * vb.x;
  float c14 = va.x * vb.w - va.w * vb.x;
  float c23 = va.y * vb.z - va.z * vb.y;
  float c24 = va.y * vb.w - va.w * vb.y;
  float c34 = va.z * vb.w - va.w * vb.z;
  float a1 = v.x;
  float a2 = v.y;
  float a3 = v.z;
  float a4 = v.w;

  vec4 res;
  res.x = a1*(c12*c12 + c13*c13 + c14*c14 - c23*c23 - c24*c24 - c34*c34)
        + a3*(-2*c12*c23 + 2*c14*c34)
        + a4*(-2*c12*c24 - 2*c13*c34);
  res.y = a1*(2*c13*c23 + 2*c14*c24)
        + a2*(c12*c12 + c23*c23 - c24*c24 - c13*c13 - c14*c14 - c34*c34)
        + a3*(2*c12*c13 + 2*c24*c34)
        + a4*(2*c12*c14 - 2*c23*c34);
  res.z = a2*(2*c12*c13)
        + a3*(c13*c13 + c23*c23 - c12*c12 - c14*c14 + c34*c34 - c24*c24)
        + a4*(2*c13*c14 + 2*c23*c24);
  res.w = a1*(-2*c12*c24)
        + a2*(2*c12*c14 - 2*c23*c24)
        + a3*(2*c13*c14 + 2*c23*c34)
        + a4*(-c12*c12 - c13*c13 - c23*c23 + c14*c14 + c24*c24 + c34*c34);
  res = res * sin2_theta + v * cos2_theta;
  return res;
}

vec4 reflect4(vec4 v, vec4 a){
  float exy = a.x * v.y - a.y * v.x;
  float exz = a.x * v.z - a.z * v.x;
  float exw = a.x * v.w - a.w * v.x;
  float eyz = a.y * v.z - a.z * v.y;
  float eyw = a.y * v.w - a.w * v.y;
  float ezw = a.z * v.w - a.w * v.z;
  float dva = dot(v, a);
  vec4 res = dva * a;
  res.x +=  exy * a.y + exz * a.z + exw * a.w;
  res.y += -exy * a.x + eyz * a.z + eyw * a.w;
  res.z += -exz * a.x - eyz * a.y + ezw * a.w;
  res.w += -exw * a.x - eyw * a.y - ezw * a.z;
  return -res;
}

vec4 rotate4(vec4 v, vec4 a, vec4 b){
  return reflect4(reflect4(v, a), b);
}

float surfaceDist(float3 p1, float3 p2){
  float4 pos4 = sphere2euc(p1);
  float4 other4 = sphere2euc(p2);
  return RADIUS_3S * (acos(dot(pos4, other4)));
}

float surfaceDist(float4 p1, float4 p2){
  return RADIUS_3S * (acos(dot(p1, p2)));
}

float surfaceDist(float4 p1, float3 p2){
  float4 p2_4 = sphere2euc(p2);
  return RADIUS_3S * (acos(dot(p1, p2_4)));
}

float sqr(float a){
  return a*a;
}

float sphereDist(float4 pos, float4 sphere_c, float r){
  float ttime = g_time * 0.1;
  // sphere_c.x += fract(ttime/21) * PI * 2;
  // sphere_c.y += fract(ttime/15) * PI * 2;
  // sphere_c.z += fract(ttime/24) * PI * 2;
  return (surfaceDist(pos, sphere_c)) - r + ttime - ttime;
}

float sphereDist(float4 pos, Sphere s){
  return sphereDist(pos, s.center, s.r);
}

float boxDist(float3 pos, float3 p, float3 b){
  float3 d = abs(p-pos) - b;
  float r = 0;
  return length(max(d, 0.0)) + min(max(d.x, max(d.y, d.z)), 0.0);
}

float boxDist(float3 pos, Box b){
  return boxDist(pos, b.p, b.b);
}

float planeDist(float4 pos, Plane pl){
  return (RADIUS_3S * (PI*0.5 - acos(dot(pl.n, pos))));
}

float csg1Dist(float3 pos, Csg cs){
  float r = max(cs.b.x, max(cs.b.y, cs.b.z));
  return -1.0f;
  // return max(boxDist(pos, cs.p, cs.b), -sphereDist(pos, cs.p, cs.k * r));
}

float mbDist(float3 pos, Mandelbulb mb, out float3 resColor){
  int MB_ITER = 4;
  float c1 = 8.0;
  float c2 = 1.0;

  float3 w = mb.pos - pos;
  float m = dot(w, w);    
  float4 trap = float4(abs(w), m);
  float dz = 1.0;

  for (int i = 0; i < MB_ITER; i++){
    dz = c1 * pow(sqrt(m), 7.0)*dz + c2;
    float r = length(w);
    float b =  acos(w.y/r);
    float a =  atan(w.x, w.z);
    w = mb.pos - pos + pow(r, 8.0) * vec3(sin(b)*sin(a), cos(b), sin(b)*cos(a));
    trap = min( trap, vec4(abs(w), m));

    m = dot(w,w);
    if(m > 256.0)
      break;
  }
  resColor = float3(m,trap.yz);

  return 0.25*log(m)*sqrt(m)/dz;
}

float DistEval(float4 pos, out int obj_type, out int obj_ind){
  float tmp;
  float mint = MAX_T;
  for (int i = 0; i < sphere_cnt; i++){
    tmp = sphereDist(pos, spheres[i]);
    if (tmp < mint){
      mint = tmp;
      obj_type = SPHERE;
      obj_ind  = i;
    }
  }

  for (int i = 0; i < 0; i++){
    tmp = boxDist(pos.xyz, boxes[i]);
    if (tmp < mint){
      mint = tmp;
      obj_type = BOX;
      obj_ind  = i;
    }
  }

  for (int i = 0; i < csg1_cnt - csg1_cnt; i++){
    tmp = csg1Dist(pos.xyz, csg1[i]);
    if (tmp < mint){
      mint = tmp;
      obj_type = CSG1;
      obj_ind  = i;
    }
  }

  for (int i = 0; i < plane_cnt; i++){
    tmp = planeDist(pos, planes[i]);
    if (tmp < mint){
      mint = tmp;
      obj_type = PLANE;
      obj_ind  = i;
    }
  }

  return max(mint, 0);
}

float typedDist(float4 pos, int obj_type, int obj_ind){
  switch (obj_type){
    case SPHERE:
      return sphereDist(pos, spheres[obj_ind]) + main_scale - main_scale;
    case BOX:
      return boxDist(pos.xyz, boxes[obj_ind]);
    case PLANE:
      return planeDist(pos, planes[obj_ind]);
    case CSG1:
      return csg1Dist(pos.xyz, csg1[obj_ind]);
    default:
      return 1e20f;
  }
}

Material typedObjInfo(float4 p, int obj_type, int obj_ind){
  float3 pos = p.xyz;
  switch (obj_type){
    case SPHERE:
      return spheres[obj_ind].mat;
    case BOX:
      return boxes[obj_ind].mat;
    case MANDELBULB:
      return BRASS;
    case PLANE:
      float3 a = euc2sphere(p);
      // if ((fract(pos.x) < 0.5 && fract(pos.z) < 0.5) ||  (fract(pos.z) > 0.5 && fract(pos.x) > 0.5))
      // if (abs(a.x) < 0.5)
        return planes[obj_ind].mat;
      // else
        // return BRASS;
    case CSG1:
      return csg1[obj_ind].mat;
    default:
      return BRASS;
  }
}

float4 probeNorm(float4 pos, int obj_type, int obj_ind){
  float4 locx, locy, locz, rotor;
  float dx, dy, dz;

  rotor = normalize(g_eucPos + pos);
  locx = rotate4(g_locX, g_eucPos, rotor);
  locy = rotate4(g_locY, g_eucPos, rotor);
  locz = rotate4(g_locZ, g_eucPos, rotor);

  float4 p1 = pos + EPS_DERIV * locx;
  float4 p2 = pos - EPS_DERIV * locx;
  float4 p3 = pos + EPS_DERIV * locy;
  float4 p4 = pos - EPS_DERIV * locy;
  float4 p5 = pos + EPS_DERIV * locz;
  float4 p6 = pos - EPS_DERIV * locz;
  
  dx = typedDist(p1, obj_type, obj_ind) - typedDist(p2, obj_type, obj_ind);
  dy = typedDist(p3, obj_type, obj_ind) - typedDist(p4, obj_type, obj_ind);
  dz = typedDist(p5, obj_type, obj_ind) - typedDist(p6, obj_type, obj_ind);
  return normalize(dx * locx + dy * locy + dz * locz);
}

RayHit closestDirectional(float4 ray_pos, float4 ray_dir){
  
  int type_tmp, obj_ind, obj_type;
  float iter_cnt, t, angle;
  float dt = MAX_T;
  float dangle = PI*2;
  
  obj_type = NOHIT;
  t = 0;
  angle = 0;
  iter_cnt = 0;
  // float3 cur_pos = ray_pos;

  // vec4 va = sphere2euc(ray_pos);
  vec4 va = ray_pos;
  vec4 vn = ray_dir;
  vec4 tmp;

  while (t < MAX_T && dt > EPS_STOP * t && iter_cnt < MAX_ITER){
    iter_cnt += 1.0f;
    dt = DistEval(va, type_tmp, obj_ind);
    t += dt;
    float dangle = dt * RADIUS_3S_INV;
    angle = t * RADIUS_3S_INV;
    if (angle >= PI * 2){
      obj_type = NOHIT;
      break;
    }
    if (dt <= EPS_STOP * t){
      obj_type = type_tmp;
      break;
    }
    float ca = cos(dangle);
    float sa = sin(dangle);
    tmp = normalize(sa*vn + ca*va);
    vn = normalize(ca*vn - sa*va);
    va = tmp;
  }
  return RayHit(obj_type, obj_ind, t, va, iter_cnt);
}

float softShadow(float3 ray_pos, float3 ray_dir, float dist){
  float r = 0.01;
  float h = 1e3;
  float iter_cnt = 0;
  float prev_dt = 1e10; 
  float t = 0, tl = 0;
  float dt = MAX_T;
  int type, ind;
  float3 cur_pos = ray_pos;
  
  while (iter_cnt < MAX_ITER){
    iter_cnt += 1.0f;
    vec4 cur_pos4 = vec4(cur_pos.xyz, 0);
    dt = DistEval(cur_pos4, type, ind);
    if( dt < EPS_STOP || t > MAX_T ) break;     

    if (dt < h){
      h = dt;
      tl = t;
    }
    // h = min(h, dt);

    // float y = dt*dt / (2.0 * prev_dt);
    // float d = sqrt(dt*dt-y*y);
    // soft = min( soft, 100000 * d/max(0.0,t-y));
    // prev_dt = dt;    
    
    t += dt;
    cur_pos = ray_pos + ray_dir * t;// * 0.99; 
  }
  return clamp(h * dist / (tl * r), 0.0, 1.0);
}

float3 localLight(float4 pos4, float4 normal, float4 reflected, int obj_type, int obj_ind, out float3 spec_out){
  float3 c = float3(0.0f, 0.0f, 0.0f);
  float k1 = 1;
  float k2 = 0.02;
  float k3 = 0.01;
  float att, dist;
  float4 in_ray;
 
  float4 L, NL;

  Material m = typedObjInfo(pos4, obj_type, obj_ind);
  spec_out = float3(0, 0, 0);

  c += m.ambient;
  
  for (int i = 0; i < LIGHTS_CNT; i++){
    vec4 light_pos = sphere2euc(g_LightSources[i]);

    L = +pos4 + reflect4(pos4, light_pos);
    NL = -normalize(L);
    in_ray = -normalize(light_pos + reflect4(light_pos, pos4));
    dist = surfaceDist(light_pos, pos4);

    RayHit rh = closestDirectional(light_pos, NL);
    float tmp_check = -sign(length(rh.pos - pos4) - (1e-1));
    tmp_check = max(tmp_check, 0);
    float dot_tmp = dot(in_ray, normal);
    att = 1;
    att = 1.0f / (k1 + dist*(k2 + k3*dist));
    
    float dif  = max(0, dot_tmp);
    float dot_check = sign(dif);
    float spec = pow(max(0, dot(in_ray, reflected)), m.shiny);
    float3 ct = att * dif * g_LightColors[i] * m.diffuse /** (1.0f - rh.iter_cnt*10/MAX_ITER)*/;
    // float3 ct = att * dif * tmp_check * vec3(1, 1, 1) * (rh.iter_cnt / MAX_ITER);
    spec_out += att * spec * m.specular * g_LightColors[i];
    c += ct;

    // light from opposite direction
    
    L = +pos4 + reflect4(pos4, light_pos);
    NL = normalize(L);
    in_ray = normalize(light_pos + reflect4(light_pos, pos4));
    dist = surfaceDist(light_pos, pos4);

    rh = closestDirectional(light_pos, NL);
    tmp_check = -sign(length(rh.pos - pos4) - (1e-1));
    tmp_check = max(tmp_check, 0);
    dot_tmp = dot(in_ray, normal);
    att = 1;
    att = 1.0f / (k1 + dist*(k2 + k3*dist));
    
    dif  = max(0, dot_tmp);
    dot_check = sign(dif);
    spec = pow(max(0, dot(in_ray, reflected)), m.shiny);
    ct = att * dif * g_LightColors[i] * m.diffuse /** (1.0f - rh.iter_cnt*10/MAX_ITER)*/;
    // float3 ct = att * dif * tmp_check * vec3(1, 1, 1) * (rh.iter_cnt / MAX_ITER);
    spec_out += att * spec * m.specular * g_LightColors[i];
    c += ct;
  }
  
  return clamp(c, 0.0f, 1.0f);
}

float3 RayMarch(float4 ray_pos, float4 ray_dir){
  float4 normal;
  float4 reflected;
  float3 tmpc;
  float4 rd, rp;
  
  float3 final_c = float3(0, 0, 0);
  float frac = 1.0f;
  float3 spec_out;

  rp = ray_pos;
  rd = ray_dir;
  for (int i = 0; i < MAX_BOUNCES; i++){
    RayHit rh = closestDirectional(rp, rd);
    if (rh.obj_type == NOHIT){
      tmpc = skyBox(rd.xyz);
      final_c += tmpc * frac;
      break;
    }else{
      //TODO
      normal = probeNorm(rh.pos, rh.obj_type, rh.obj_ind);

      reflected = reflect4(-normalize(rotate4(rd, rp, normalize(rp+rh.pos))), normal);
      tmpc = localLight(rh.pos, normal, reflected, rh.obj_type, rh.obj_ind, spec_out);
      // tmpc = normal.xyz;
    }
    Material m = typedObjInfo(rh.pos, rh.obj_type, rh.obj_ind);
    // tmpc.g *= exp(-0.05*rh.dist);
    // tmpc.r *= exp(-0.1*rh.dist);
    final_c += (tmpc * (1 - m.reflection) + spec_out) * frac;// * (rh.iter_cnt / MAX_ITER);
    frac *= m.reflection;
    rp = rh.pos + normal * max(EPS_STOP, EPS_DERIV) * 100;
    rd = reflected;
  }

  return final_c;
}

void main(void){ 

  float w = float(g_screenWidth);
  float h = float(g_screenHeight);
 
  float x = fragmentTexCoord.x*w; 
  float y = fragmentTexCoord.y*h;

  float3 ray_pos, color;
  float4 ray_dir;

  float angle = g_time * 0.1;
  spheres[1].center = float4(0, sin(angle), cos(angle), 0);

  if (ALIASING == 1){
    ray_dir = EyeRayDir(x,y,w,h, float3(0, 0, 0));

    color = RayMarch(g_eucPos, ray_dir);
    fragColor = float4(color.xyz, 1.0);
    return;
  }

   
  color = float3(0, 0, 0);
  for (int i = 0; i < ALIASING; i++){
    for (int j = 0; j < ALIASING; j++){
      float3 offset = float3(i, j, 0)/ALIASING - 0.5;

      ray_dir = EyeRayDir(x, y, w, h, offset);

      color += RayMarch(g_eucPos, ray_dir);
    } 
  }
  color /= ALIASING * ALIASING;
  fragColor = float4(color.xyz, 1.0);

}


